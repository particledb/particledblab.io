let onessg = require('onessg'),
    express = require('express'),
    watch = require('node-watch'),
    path = require('path'),
    _

let ssgcfg = {
    src: 'src/',
    dist: 'dist/',
    layouts: 'layouts/'
}

function generate() { onessg(ssgcfg).catch(err => console.error(err)) }
generate()

let app = express()
;['dist', 'www'].map(x => app.use(express.static(path.join(__dirname, x))))

watch(['src', 'layouts'].map(x => path.join(__dirname, x)), { recursive: true }, function(evt, name) {
    console.log('%s changed.', name)
    generate()
})

app.listen(1337)