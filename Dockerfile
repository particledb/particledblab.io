FROM node:10 as webgen

ADD layouts /src/layouts
ADD src /src/src
ADD www /src/www
ADD index.js package.json package-lock.json /src/

WORKDIR /src
RUN npm ci ;\
    cp -R www public ;\
    npm run-script gitlab-pages

FROM golang:1.12 as serve

WORKDIR /build
ADD serve .

RUN CGO_ENABLED=0 GOOS=linux go build -a

FROM alpine

WORKDIR /www
COPY --from=webgen /src/public .

WORKDIR /bin
COPY --from=serve /build/serve .

EXPOSE 80
CMD [ "/bin/serve", "/www" ]