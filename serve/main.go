package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
	"strings"
	"text/template"
)

const templateSrcGoDoc = `<a href="https://godoc.org/particledb.io/{{.Project}}{{.Package}}>Found</a>`

const templateSrcGoImport = `<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
		<meta name="go-import" content="particledb.io/{{.Project}} git https://gitlab.com/particledb/{{.Project}}">
		<meta http-equiv="refresh" content="0; url=https://godoc.org/particledb.io/{{.Project}}{{.Package}}">
	</head>
	<body>
		Redirecting to docs at <a href="https://godoc.org/particledb.io/{{.Project}}{{.Package}}">godoc.org/particledb.io/{{.Project}}{{.Package}}</a>...
	</body>
</html>`

var templateGoDoc, templateGoImport *template.Template

type packageHandler string

func (b packageHandler) ServeHTTP(resp http.ResponseWriter, req *http.Request) {
	var data struct{ Project, Package string }
	var base = string(b)

	if !strings.HasPrefix(req.URL.Path, "/"+base) {
		log.Println("invalid request path")
		resp.WriteHeader(500)
		fmt.Fprintln(resp, "A server error occurred")
		return
	}

	data.Project = base
	data.Package = req.URL.Path[len(base)+1:]

	resp.Header().Add("Content-Type", "text/html; charset=utf-8")

	keys, ok := req.URL.Query()["go-get"]
	if !ok || len(keys) == 0 || keys[0] != "1" {
		resp.Header().Add("Location", fmt.Sprintf("https://godoc.org/particledb.io/%s%s", data.Project, data.Package))
		resp.WriteHeader(302)
		if err := templateGoDoc.Execute(resp, data); err != nil {
			log.Println(err)
		}
		return
	}

	if err := templateGoImport.Execute(resp, data); err != nil {
		log.Println(err)
	}
}

func addPackageHandler(base string) {
	http.Handle("/"+base, packageHandler(base))
	http.Handle("/"+base+"/", packageHandler(base))
}

func main() {
	var err error

	if len(os.Args) != 2 {
		log.Println("usage: serve /www")
		os.Exit(1)
		return
	}

	if templateGoDoc, err = template.New("go-import").Parse(templateSrcGoDoc); err != nil {
		log.Println(err)
		os.Exit(2)
		return
	}

	if templateGoImport, err = template.New("go-import").Parse(templateSrcGoImport); err != nil {
		log.Println(err)
		os.Exit(2)
		return
	}

	http.Handle("/", http.FileServer(http.Dir(os.Args[1])))

	addPackageHandler("base")
	addPackageHandler("structs")
	addPackageHandler("scheme")
	addPackageHandler("core")
	addPackageHandler("service")

	port := os.Getenv("PORT")
	if port == "" {
		port = "80"
	}

	err = http.ListenAndServe(":"+port, nil)
	if err != nil {
		log.Println(err)
	}
}
